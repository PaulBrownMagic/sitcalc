/*
In this example we have one actor, tony, plus a door and a light to work with.

Example queries:

```
?- situation::new([door_position(closed), power(light, off)], s0).
true.

?- s0::holds(door_position(Pos) and power(light, Power)).
Pos = closed,
Power = off ;
false.

?- s0::poss(A).
A = open_door ;
A = turn_on(_16992) ;
false.

?- situation::do(open_door, s0, s1).
true.

?- s1::history(H).
H = [instance(o3, open_door, [time_(date_time(date(2019, 10, 3), time(9, 38, 27)))])].

?- tony::do(turn_on(light), s1, s2).
true.

?- s2::history(H).
H = [instance(o4, turn_on(light), [time_(date_time(date(2019, 10, 3), time(9, 38, 49))), instance(tony, actor, [name('Tony')])]),
     instance(o3, open_door, [time_(date_time(date(2019, 10, 3), time(9, 38, 27)))])] .

?- o4::repr(R).
R = instance(o4, turn_on(light), [time_(date_time(date(2019, 10, 3), time(9, 38, 49))), instance(tony, actor, [name('Tony')])]) .

?- s2::prior(S).
S = s1 ;
S = s0 ;
false.

?- s2::holds(F), F::holds(s1).
F = door_position(open) ;
false.
```
*/

:- object(open_door,
    instantiates(meta_action),
    specializes(action)).
    poss_when(door_position(closed)).
:- end_object.

:- object(close_door,
    instantiates(meta_action),
    specializes(action)).
    poss_when(door_position(open)).
:- end_object.

:- object(turn_on(_PoweredEntity_),
    instantiates(meta_action),
    specializes(action)).
    poss_when(power(_PoweredEntity_, off)).
:- end_object.

:- object(turn_off(_PoweredEntity_),
    instantiates(meta_action),
    specializes(action)).
    poss_when(power(_PoweredEntity_, on)).
:- end_object.

:- object(door_position(_Value_),
    extends(fluent)).
    value(Sit) :-
        Sit::action_class(A),
        Sit::prior(Prior),
        ( A == open_door, _Value_ = open
        ; A == close_door, _Value_ = closed
        ; A \= open_door, A \= close_door,
          Prior::holds(door_position(_Value_))
        ).
:- end_object.

:- object(power(_PoweredEntity_, _Value_),
    extends(fluent)).
    value(Sit) :-
        Sit::action_class(A),
        Sit::prior(Prior),
        ( A = turn_on(_PoweredEntity_), _Value_ = on
        ; A = turn_off(_PoweredEntity_), _Value_ = off
        ; A \= turn_on(_PoweredEntity_), A \= turn_off(_PoweredEntity_),
          Prior::holds(power(_PoweredEntity_, _Value_))
        ).
:- end_object.

:- object(tony,
    instantiates(actor)).
    name('Tony').
:- end_object.
