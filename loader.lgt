:- initialization((
	logtalk_load_context(directory, Directory),
	assertz(logtalk_library_path(situations, Directory)),
    logtalk_load(
        [ meta(loader)
        , dates(loader)
        , hierarchies(loader)
        , situations(situations)
        ],
        [
        ])
)).

