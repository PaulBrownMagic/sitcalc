:- object(tests_actions,
    extends(lgtunit)).

	:- info([
		version is 1.0,
		author is 'Paul Brown',
		date is 2019/10/03,
		comment is 'Unit tests for the action classes and instances.'
	]).

    setup :-
        situation::new([door_position(closed), power(light, off)], s0),
        situation::do(open_door, s0, s1),
        situation::do(turn_on(light), s1, s2),
        situation::do(turn_off(light), s2, s3),
        tony::do(close_door, s3, s4).

    cleanup :-
        abolish_object(s0),
        s1::action(A1), abolish_object(A1), abolish_object(s1),
        s2::action(A2), abolish_object(A2), abolish_object(s2),
        s3::action(A3), abolish_object(A3), abolish_object(s3),
        s4::action(A4), abolish_object(A4), abolish_object(s4).

	cover(action).
	cover(meta_action).

    test(factory_new, true) :- action::current_predicate(new/1).
    test(factory_new_with_actor, true) :- action::current_predicate(new/2).
    test(factory_has_poss, true) :- action::current_predicate(poss/1).

    test(action_has_poss, true) :- s1::action(A), A::current_predicate(poss/1).
    test(poss_and_poss, true) :- s1::action(A), A::poss(s0), open_door::poss(s0).

    test(make_new, true) :- open_door::new(_), turn_on(light)::new(tony, _).

    test(has_actor, true(Actor == tony)) :-
        s4::action(A), A::actor(Actor).

    test(datetimes, true([D1, T1] == [D2, T2])) :-
        turn_off(light)::new(A),
        A::at_datetime(datetime(D1, T1)),
        A::at_date(D2),
        A::at_time(T2).

    test(repr, true(Repr = instance(_, open_door, [time_(datetime(_, _))]))) :-
        s1::action(A),
        A::repr(Repr).

    test(repr_actor, true(Repr = instance(_, close_door, [actor(_), time_(datetime(_, _))]))) :-
        s4::action(A),
        A::repr(Repr).

:- end_object.
