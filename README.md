# Situations

This is a Situation Calculus based framework. You can use it to reason about
changes in situations brought about by actions.

To use it you extend the fluents prototype and specialize the actions
class for those fluents and actions in your domain. You can also
instantiate the actors class so you can track who does the actions.


## Example Use

A simple example can be found in the aptly named `simple_example.lgt`.

```
$~: logtalk loader.pl

[...loading blurb...]

?- {simple_example}.
true.

?- situation::new([door_position(closed), power(light, off)], s0).
true.

?- s0::holds(door_position(Pos) and power(light, Power)).
Pos = closed,
Power = off ;
false.

?- s0::poss(A).
A = open_door ;
A = turn_on(_16992) ;
false.

?- situation::do(open_door, s0, s1).
true.

?- s1::history(H).
H = s0 ;
false.

?- tony::do(turn_on(light), s1, s2).
true.

?- s2::history(H).
H = s1 ;
H = s0 ;
false.

?- s2::prior(S).
S = s1 .

?- o4::repr(R).
R = instance(o4, turn_on(light), [actor(tony), time_(date_time(date(2019, 10, 3), time(9, 38, 49, 439)))]) .

?- s2::holds(F), F::holds(s1).
F = door_position(open) ;
false.
```

## Objects and Situation Calculus

We have 4 kinds of objects that we use with this variation of Situation
Calculus. We'll inspect them in turn.

### Situation

`situation` is the class of all situations, whereas `s0` would be an
instance of a situation. We can use the `situation` class object to
create new situations, in which nothing has yet happened. To do this you
need to define the fluents that hold in the initial situation, which
could be none:

```
?- situation::new([], s0).
true.
```
or
```
?- situation::new([door_position(closed), power(light, off)], s0).
true.
```

We're using the idea's put forth by Raymond Reiter to work out what
holds in a situation, we regress over the history of the situations to
determine the current value of a fluent.

This is a lazy approach, so fluents that aren't queried aren't
calculated, which makes `do/3-4` more efficient. However, when
situations get long then it can start to become slow when querying
`Sit::holds(Fluent)`. This can be circumvented by using a Prolog which
supports tabling.

`prior/1` yields the prior situation object, `history/1` is the
transitive version of `prior/1`.

```
?- situation::do(open_door, s0, s1).
true.   % holding fluents updated now

?- s0::holds(F), s1::holds(F).
F = power(light, off) ;
false.

?- s1::prior(P).
P = s0.

?- s1::history(H).
H = s0 ;
false.
```

Other useful predicates are provided including:
- `action/1` for accessing the action done in the situation
- `poss/1` for checking what actions are possible in a situation
- `holds/1` for checking what fluents hold in a situation

Furthermore, `holds/1` will accept logical queries provided no fluents
are holey variables, so `s0::holds(door_position(Pos) and F)` will fail,
but `s0::holds(door_position(Pos) and power(light, Pow))` will succeed.
The supported operators are: `and`, `or`, `implies`, `not` and
`equivalentTo`.

### Actions

In situation calculus no distinction is made between a kind of an action
and an instance of one. We do. We provide an `action` class that can be
specialized, as per `open_door` or `turn_on(light)` in `simple_example`.
These are kinds of actions that are defined by the pre- and
post-conditions. When an action is actually executed then an instance of
that class is made with additional information such as the time the
action was created and if some actor is attributed to it, then who that
actor is.

### Fluents

A fluent is something that is true in some situations. It is a
relationship between some things, like an entity and some quality of
that entity. They're up to you to define in your application, you can
follow the fluents in the examples. We define them as prototypes as
there is no distinction between kind and instance of a fluent.

In this framework a fluent is identified by the relationship, if you
need to distinguish between entities it must be done within the fluent
object.


### Actors

These don't explicitly appear in situation calculus. Not all changes
between situations occur as a result of an action by an actor, hence we
provide `situation::do/3`. But some do and sometimes you want to track
who did what. So we provide `situation::do/4` but also an actor class.
This can be instantiated, as we do with `tony` in `simple_example`. Then
you can write `tony::do(open_door, s0, s1)` and `tony` will be
attributed with being the actor responsible for executing the action.
