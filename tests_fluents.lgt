:- object(tests_fluents,
	extends(lgtunit)).

	:- info([
		version is 1.0,
		author is 'Paul Brown',
		date is 2019/10/03,
		comment is 'Unit tests for the fluent prototypes.'
	]).

    setup :-
        situation::new([door_position(closed), power(light, off)], s0),
        situation::do(open_door, s0, s1),
        situation::do(turn_on(light), s1, s2),
        situation::do(turn_off(light), s2, s3),
        situation::do(close_door, s3, s4).

    cleanup :-
        abolish_object(s0),
        s1::action(A1), abolish_object(A1), abolish_object(s1),
        s2::action(A2), abolish_object(A2), abolish_object(s2),
        s3::action(A3), abolish_object(A3), abolish_object(s3),
        s4::action(A4), abolish_object(A4), abolish_object(s4).

	cover(fluent).

    test(has_holds, true) :- door_position(_)::current_predicate(holds/1).
    test(has_value, true) :- door_position(_)::current_predicate(value/1).

    test(s0_holds, true([Position, Entity, Power] == [closed, light, off])) :-
        door_position(Position)::holds(s0),
        power(Entity, Power)::holds(s0).

    test(s1_holds, true([Position, Entity, Power] == [open, light, off])) :-
        door_position(Position)::holds(s1),
        power(Entity, Power)::holds(s1).

    test(s2_holds, true([Position, Entity, Power] == [open, light, on])) :-
        door_position(Position)::holds(s2),
        power(Entity, Power)::holds(s2).

    test(s3_holds, true([Position, Entity, Power] == [open, light, off])) :-
        door_position(Position)::holds(s3),
        power(Entity, Power)::holds(s3).

    test(s4_holds, true([Position, Entity, Power] == [closed, light, off])) :-
        door_position(Position)::holds(s4),
        power(Entity, Power)::holds(s4).

    test(value_first_action, true([P1, P2, P3, P4] == [off, on, open, closed])) :-
        power(light, P1)::value(s3),
        power(light, P2)::value(s2),
        door_position(P3)::value(s1),
        door_position(P4)::value(s4).

    test(irrelevant_actiont_to_s0,  true([Entity, Power] == [light, off])) :-
        power(Entity, Power)::value(s1).

    test(irrelevant_action,  true([Position, Entity, Power] == [open, light, off])) :-
        door_position(Position)::value(s3),
        power(Entity, Power)::value(s4).

    test(can_find_extensions, true(Set = [door_position(_), power(_, _)])) :-
        setof(F, fluent::extension(F), Set).

:- end_object.
