:- initialization((
	set_logtalk_flag(report, warnings),
    logtalk_load(
        [ meta(loader)
        , dates(loader)
        , hierarchies(loader)
        ]),
	logtalk_load(lgtunit(loader)),
	logtalk_load(
        [ situations
        ],
        [ source_data(on)
        , debug(on)
        ]),
	logtalk_load(simple_example),
	logtalk_load(
        [ tests_fluents
        , tests_actions
        , tests_situations
        ],
        [ hook(lgtunit)
        ]),
    lgtunit::run_test_sets(
        [ tests_fluents
        , tests_actions
        , tests_situations
        ])
)).
