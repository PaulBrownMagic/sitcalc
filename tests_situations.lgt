:- object(tests_situations,
    extends(lgtunit)).

	:- info([
		version is 1.0,
		author is 'Paul Brown',
		date is 2019/10/03,
		comment is 'Unit tests for the situation classes and instances.'
	]).

    setup :-
        situation::new([door_position(closed), power(light, off)], s0),
        situation::do(open_door, s0, s1),
        situation::do(turn_on(light), s1, s2),
        situation::do(turn_off(light), s2, s3),
        tony::do(close_door, s3, s4).

    cleanup :-
        abolish_object(s0),
        s1::action(A1), abolish_object(A1), abolish_object(s1),
        s2::action(A2), abolish_object(A2), abolish_object(s2),
        s3::action(A3), abolish_object(A3), abolish_object(s3),
        s4::action(A4), abolish_object(A4), abolish_object(s4).

	cover(situation).
	cover(metasituation).
    cover(actor).

    test(actor_name, true(Name == 'Tony')) :-
        tony::name(Name).

    test(factory_new, true) :- situation::current_predicate(new/2).
    test(factory_has_do, true) :- situation::current_predicate(do/3), situation::current_predicate(do/4).

    test(mk_new_empty, true) :-
        situation::new([], S), \+ S::holds(_).
    test(do_3, true([Action, Prior] == [open_door, s4])) :-
        situation::do(open_door, s4, S),
        S::action(A),
        instantiates_class(A, Action),
        S::prior(Prior).
    test(do_4_actor, true(Actor == tony)) :-
        tony::do(turn_on(light), s4, S),
        S::action(A),
        A::actor(Actor).

    test(no_action, fail) :-
        s0::action(_).

    test(action, true(A == open_door)) :-
        s1::action(Action),
        instantiates_class(Action, A).

    test(empty_history, fail) :-
        s0::history(H).
    test(history_prior_sit, true(S == s0)) :-
        s1::history(S).
    test(transitive_history, true) :-
        s4::history(s0).

    test(no_prior, fail) :-
        s0::prior(_).
    test(prior_one, true) :-
        s4::prior(s3).

    test(holds_var, true(Set == [door_position(closed), power(light, off)])) :-
        setof(F, s0::holds(F), Set).
    test(holds_nonvar, true(Pos == open)) :-
        s2::holds(not (not door_position(Pos))).
    test(holds_query_conj, true([Pos, Pow] == [open, on])) :-
        s2::holds(door_position(Pos) and power(light, Pow)).
    test(holds_implies, true(Pos == closed)) :-
        s0::holds(power(light, off) implies door_position(Pos)).
    test(holds_eq, true) :-
        s0::holds(power(light, on) equivalentTo door_position(open)).
    test(holds_not_eq, true) :-
        s0::holds(not (power(light, off) equivalentTo door_position(open))).
    test(holds_not_implies, true) :-
        s0::holds(not (power(light, off) implies door_position(open))).
    test(holds_query_not_conj, true) :-
        s2::holds(not (door_position(closed) and power(light, off))).
    test(holds_compound_query_or, true) :-
        s2::holds(door_position(closed) or door_position(open)).

    test(poss_ground, true) :-
        s0::poss(open_door).
    test(not_poss, fail) :-
        s0::poss(close_door).
    test(all_poss, true(Set = [close_door, turn_on(_)])) :-
        setof(A, s3::poss(A), Set).

    test(repr_props_empty, true(P == [])) :-
        situation::new([], S),
        S::repr_props(P).

    test(repr_props_s0, true(Props == [holds_(door_position(closed)),holds_(power(light,off))])) :-
        s0::repr_props(Props).

    test(repr_props_s1, true(Props = [action(_),prior_(s0)])) :-
        s1::repr_props(Props).

    test(repr_s0, true(R = instance(s0,situation,[holds_(door_position(closed)),holds_(power(light,off))]))) :-
        s0::repr(R).

:- end_object.
